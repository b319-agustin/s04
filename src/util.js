const names = {
	"Brandon": {
		"name" : "Brandon Boyd",
		"age" : 35
	}, 
	"Steve" : {
		"name" : "Steve Tyler",
		"age" : 56
	}
}

const users = [


	{
		username: "brBoyd87",
		password: "87brandon19"

	},
	{
		username: "tylerofsteve",
		password: "stevenstyle75"
	}

]

function factorial(n){
	if(typeof n !== 'number') return undefined;
	if(n < 0) return undefined;
	if(n===0) return 1;
	if(n===1) return 1;
	return n * factorial(n-1);
}

module.exports = {
	factorial: factorial,
	names: names,
	users: users
}